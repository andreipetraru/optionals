package optionals.model;

import lombok.Data;

@Data
public class Department {
    private String name;
    private Manager manager;
}
