package optionals.model;

import lombok.Data;

@Data
public class Manager {
    private String name;
    private int salary = 1_000_000_000;
}
