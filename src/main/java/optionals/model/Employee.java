package optionals.model;

import lombok.Data;

@Data
public class Employee {
    private String name;
    private Department department;
}
