package optionals.service;

import optionals.model.Employee;

public class EmployeeService {
    public String getManagerName(Employee employee) {
        return employee.getDepartment().getManager().getName();
    }
}
