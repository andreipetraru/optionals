package optionals.service;

import optionals.bettermodel.AnimalType;
import optionals.bettermodel.Person;
import optionals.bettermodel.Pet;
import org.assertj.core.util.Preconditions;

public class PersonService {
    public String getAnimalType(Person person) {
        Preconditions.checkNotNull(person, "Can't have pet if I don't exist");
        return person.getPet()
                .flatMap(Pet::getAnimalType)
                .map(AnimalType::getType)
                .orElse(defaultPet());
    }

    public String defaultPet() {
        return "O ceapa degerata";
    }
}
