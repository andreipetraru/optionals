package optionals.bettermodel;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Setter
public class Pet {
    private AnimalType animalType;

    public Optional<AnimalType> getAnimalType() {
        return Optional.ofNullable(animalType);
    }
}
