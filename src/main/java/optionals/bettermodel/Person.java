package optionals.bettermodel;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@AllArgsConstructor
@NoArgsConstructor
@Setter
public class Person {
    private Pet pet;

    public Optional<Pet> getPet() {
        return Optional.ofNullable(pet);
    }
}
