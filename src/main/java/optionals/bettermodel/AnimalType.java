package optionals.bettermodel;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
public class AnimalType {
    private String type;

    public String getType() {
        return type;
    }
}
