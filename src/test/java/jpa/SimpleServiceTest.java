package jpa;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class SimpleServiceTest {
    private static EntityManager entityManager;

    @BeforeAll
    public static void init() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("simpleJPA");
        entityManager = emf.createEntityManager();
    }

    @Test
    public void trySave() {
        entityManager.getTransaction().begin();
        entityManager.persist(null);
        entityManager.getTransaction().commit();
    }
}
