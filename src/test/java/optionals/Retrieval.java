package optionals;

import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.function.Consumer;

public class Retrieval {
    @Test
    public void get() {
        Optional<String> empty = emptyOptional();
        empty.get();
    }

    @Test
    public void ifPresent() {
        Optional<String> empty = emptyOptional();
        empty.ifPresent(System.out::println);
    }

    @Test
    public void ifPresentOrElse() {
        Optional<String> empty = emptyOptional();
        Consumer<String> consumer = s -> {
            System.out.println(Thread.currentThread().getId());
            System.out.println("This is the consumer " + s);
        };
        empty.ifPresentOrElse(consumer, () -> {
            System.out.println("Other thread");
            System.out.println(Thread.currentThread().getId());
        });
    }

    @Test
    public void or() {
        Optional<String> optional = emptyOptional();
        Optional<String> or = optional.or(() -> Optional.of("OR"));
        System.out.println(or.get());
    }

    @Test
    public void orElse() {
        Optional<String> emptyOptional = emptyOptional();
        String orElse = emptyOptional.orElse("orElse");
        System.out.println(orElse);
    }

    @Test
    public void orElseGet() {
        Optional<String> empty = emptyOptional();
        String orElseGet = empty.orElseGet(() -> {
            System.out.println("Sleeping");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "orElseGet";
        });
        System.out.println(orElseGet);
    }

    @Test
    public void orElseThrow() {
        Optional<String> empty = emptyOptional();
        empty.orElseThrow(IllegalStateException::new);
    }

    @Test
    public void orElseThrowDefault() {
        Optional<String> empty = emptyOptional();
        empty.orElseThrow();
    }

    private static Optional<String> optional() {
        return Optional.of("some value");
    }

    private static Optional<String> emptyOptional() {
        return Optional.empty();
    }
}
