package optionals;

import optionals.bettermodel.AnimalType;
import optionals.bettermodel.Person;
import optionals.bettermodel.Pet;
import optionals.model.Employee;
import optionals.service.EmployeeService;
import optionals.service.PersonService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ServiceTest {
    private PersonService personService = new PersonService();
    private EmployeeService employeeService = new EmployeeService();

    @Test
    public void npe() {
        Assertions.assertThrows(NullPointerException.class, () -> employeeService.getManagerName(null));
    }

    @Test
    public void moreNpe() {
        Assertions.assertThrows(NullPointerException.class, () -> employeeService.getManagerName(new Employee()));
    }

    @Test
    public void tryNpe() {
        Assertions.assertThrows(NullPointerException.class, () -> personService.getAnimalType(null));
    }

    @Test
    public void noNpe() {
        String animalType = personService.getAnimalType(new Person());
        Assertions.assertEquals(animalType, personService.defaultPet());
    }

    @Test
    void noNpeV2() {
        String animalType = personService.getAnimalType(new Person(new Pet()));
        Assertions.assertEquals(animalType, personService.defaultPet());

    }

    @Test
    public void noNpeV3() {
        String animalType = personService.getAnimalType(new Person(new Pet(new AnimalType())));
        Assertions.assertEquals(animalType, personService.defaultPet());
    }

    @Test
    public void nonDefaultValue() {
        String animalType = personService.getAnimalType(new Person(new Pet(new AnimalType("monkey"))));
        Assertions.assertEquals(animalType, "monkey");
    }
}
